package ru.appsmart.testtask.services

import org.modelmapper.ModelMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import ru.appsmart.testtask.exceptions.ProductNotFoundException
import ru.appsmart.testtask.models.Product
import ru.appsmart.testtask.repo.PostgreProductRepo
import java.time.LocalDateTime

@Service
@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
class PostgreProductService @Autowired constructor(
        private val customerService: CustomerService,
        private val productRepo: PostgreProductRepo,
        private val modelMapper: ModelMapper
) : ProductService {

    private val log = LoggerFactory.getLogger(PostgreProductService::class.java)

    override fun getByCustomerId(customerId: Long, pageable: Pageable): Page<Product> {
        customerService.getCustomerById(customerId)
        return productRepo.findByCustomerId(customerId, pageable)
    }

    @Transactional
    override fun addProduct(customerId: Long, product: Product): Product {
        product.customer = customerService.getCustomerById(customerId)
        product.createdAt = LocalDateTime.now()
        val addedProduct = productRepo.save(product)
        log.info("$product was added")
        return addedProduct
    }

    @Transactional
    override fun deleteProductById(id: Long) : Product {
        val deletedProduct = getProductById(id)
        deletedProduct.isDeleted = true
        log.info("$deletedProduct was deleted")
        return deletedProduct
    }

    override fun getProductById(id: Long): Product {
        return productRepo.findById(id).orElseThrow { ProductNotFoundException("Product with id:$id doesn't exist") }
    }

    @Transactional
    override fun editProduct(productId: Long, product: Product): Product {
        val updatedProduct = getProductById(productId)
        modelMapper.map(product, updatedProduct)
        updatedProduct.modifiedAt = LocalDateTime.now()
        log.info("$updatedProduct was updated")
        return updatedProduct
    }
}