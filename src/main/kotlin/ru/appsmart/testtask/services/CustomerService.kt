package ru.appsmart.testtask.services

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import ru.appsmart.testtask.exceptions.CustomerNotFoundException
import ru.appsmart.testtask.models.Customer

interface CustomerService {

    fun getAll(pageable: Pageable) : Page<Customer>

    fun addCustomer(customer: Customer) : Customer

    fun deleteCustomerById(id: Long) : Customer

    fun getCustomerById(id: Long) : Customer

    fun editCustomer(id: Long, customer: Customer) : Customer

}