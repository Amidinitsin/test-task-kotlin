package ru.appsmart.testtask.services

import org.modelmapper.ModelMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import ru.appsmart.testtask.exceptions.CustomerNotFoundException
import ru.appsmart.testtask.models.Customer
import ru.appsmart.testtask.repo.PostgreCustomerRepo
import java.time.LocalDateTime

@Service
@Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
class PostgreCustomerService @Autowired constructor(
        private val customerRepo: PostgreCustomerRepo,
        private val modelMapper: ModelMapper
) : CustomerService {

    private val log = LoggerFactory.getLogger(PostgreCustomerService::class.java)

    override fun getAll(pageable: Pageable): Page<Customer> {
       return customerRepo.findAll(pageable)
    }

    @Transactional
    override fun addCustomer(customer: Customer): Customer {
        customer.createdAt = LocalDateTime.now()
        val addedCustomer = customerRepo.save(customer)
        log.info("$addedCustomer was added")
        return addedCustomer
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    override fun deleteCustomerById(id: Long) : Customer {
        val deletedCustomer = getCustomerById(id)
        deletedCustomer.isDeleted = true
        log.info("$deletedCustomer was deleted")
        return deletedCustomer
    }

    @Transactional
    override fun getCustomerById(id: Long): Customer {
        return customerRepo.findById(id).orElseThrow { CustomerNotFoundException("Customer with id: $id doesn't exist") }
    }

    @Transactional
    override fun editCustomer(id: Long, customer: Customer) : Customer {
        val updatedCustomer = getCustomerById(id)
        modelMapper.map(customer, updatedCustomer)
        updatedCustomer.modifiedAt = LocalDateTime.now()
        log.info("$updatedCustomer was updated")
        return updatedCustomer
    }
}