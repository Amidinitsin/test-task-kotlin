package ru.appsmart.testtask.services

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import ru.appsmart.testtask.exceptions.CustomerNotFoundException
import ru.appsmart.testtask.exceptions.ProductNotFoundException
import ru.appsmart.testtask.models.Product

interface ProductService {

    fun getByCustomerId(customerId: Long, pageable: Pageable) : Page<Product>

    fun addProduct(customerId: Long, product: Product) : Product

    fun deleteProductById(id: Long) : Product

    fun getProductById(id: Long) : Product

    fun editProduct(productId: Long, product: Product) : Product

}