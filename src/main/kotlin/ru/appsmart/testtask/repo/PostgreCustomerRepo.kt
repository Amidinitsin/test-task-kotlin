package ru.appsmart.testtask.repo

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import ru.appsmart.testtask.models.Customer

@Repository
interface PostgreCustomerRepo : CrudRepository<Customer, Long>{

    fun findAll(pageable: Pageable) : Page<Customer>

}