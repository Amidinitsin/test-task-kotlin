package ru.appsmart.testtask.repo

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import ru.appsmart.testtask.models.Product

@Repository
interface PostgreProductRepo : CrudRepository<Product, Long>{

    fun findByCustomerId(customerId: Long, pageable: Pageable) : Page<Product>

}