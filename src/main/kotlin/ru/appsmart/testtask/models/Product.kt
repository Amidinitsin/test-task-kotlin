package ru.appsmart.testtask.models

import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "Products")
data class Product(
        var title: String?,
        var price: BigDecimal?,
        var createdAt: LocalDateTime?
) {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null
    var description: String? = null
    @ManyToOne(cascade = [CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH])
    var customer: Customer? = null
    var isDeleted: Boolean = false
    var modifiedAt: LocalDateTime? = null

}