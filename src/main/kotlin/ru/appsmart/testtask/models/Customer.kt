package ru.appsmart.testtask.models

import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "Customers")
data class Customer(
        var name: String,
        var createdAt: LocalDateTime?
) {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null
    var isDeleted: Boolean = false
    var modifiedAt: LocalDateTime? = null

}