package ru.appsmart.testtask.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import ru.appsmart.testtask.exceptions.CustomerNotFoundException
import ru.appsmart.testtask.exceptions.ProductNotFoundException
import ru.appsmart.testtask.models.Product
import ru.appsmart.testtask.services.ProductService

@RestController
class ProductController @Autowired constructor(
        private val productService: ProductService
) {

    @GetMapping("customers/{customerId}/products")
    fun getProduct(@PathVariable("customerId") customerId: Long, pageable: Pageable): ResponseEntity<Page<Product>> {
        try {
            return ResponseEntity.ok(productService.getByCustomerId(customerId, pageable))
        } catch (e: ProductNotFoundException) {
            throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    null,
                    e
            )
        }
    }

    @GetMapping("/products/{productId}")
    fun getProductById(@PathVariable("productId") id: Long): ResponseEntity<Product> {
        try {
            return ResponseEntity.ok(productService.getProductById(id))
        } catch (e: ProductNotFoundException) {
            throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    null,
                    e
            )
        }
    }

    @PostMapping("customers/{customerId}/products")
    fun addProduct(@PathVariable("customerId") customerId: Long, @RequestBody product: Product): ResponseEntity<Product> {
        try {
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(productService.addProduct(customerId, product))
        } catch (e: CustomerNotFoundException) {
            throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    null,
                    e
            )
        }
    }

    @DeleteMapping("/products/{productId}")
    fun deleteProductById(@PathVariable("productId") productId: Long): ResponseEntity<Product> {
        return ResponseEntity.ok(productService.deleteProductById(productId))
    }

    @PutMapping("/products/{productId}")
    fun editProduct(@PathVariable("productId") productId: Long, @RequestBody product: Product): ResponseEntity<Product> {
        try {
            return ResponseEntity.ok(productService.editProduct(productId, product))
        } catch (e: ProductNotFoundException) {
            throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    null,
                    e
            )
        }

    }

}