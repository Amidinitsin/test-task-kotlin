package ru.appsmart.testtask.controllers

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import ru.appsmart.testtask.exceptions.CustomerNotFoundException
import ru.appsmart.testtask.models.Customer
import ru.appsmart.testtask.services.CustomerService

@RestController
@RequestMapping("/customers")
class CustomerController @Autowired constructor(
        private val customerService: CustomerService
) {

    @GetMapping
    fun getAllCustomers(pageable: Pageable) : ResponseEntity<Page<Customer>> {
        return ResponseEntity.ok(customerService.getAll(pageable))
    }

    @GetMapping("/{customerId}")
    fun getCustomerById(@PathVariable customerId: Long) : ResponseEntity<Customer> {
        try {
            return ResponseEntity.ok(customerService.getCustomerById(customerId))
        } catch (e: CustomerNotFoundException) {
            throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    null,
                    e
            )
        }
    }

    @DeleteMapping("/{customerId}")
    fun deleteCustomerById(@PathVariable customerId: Long) : ResponseEntity<Customer> {
        try {
            return ResponseEntity.ok(customerService.deleteCustomerById(customerId))
        } catch (e: CustomerNotFoundException) {
            throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    null,
                    e
            )
        }
    }

    @PostMapping
    fun addCustomer(@RequestBody customer: Customer) : ResponseEntity<Customer> {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(customerService.addCustomer(customer))
    }

    @PutMapping("/{customerId}")
    fun editCustomer(@PathVariable customerId: Long, @RequestBody newCustomer: Customer) : ResponseEntity<Customer> {
        try {
            return ResponseEntity.ok(customerService.editCustomer(customerId, newCustomer))
        } catch (e: CustomerNotFoundException){
            throw ResponseStatusException(
                    HttpStatus.NOT_FOUND,
                    null,
                    e
            )
        }
    }




}