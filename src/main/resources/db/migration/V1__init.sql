CREATE TABLE public.customers (
    id BIGINT NOT NULL,
    name varchar(255) NOT NULL,
    is_deleted boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    modified_at timestamp without time zone,
    PRIMARY KEY (id)
);


CREATE TABLE public.products (
    id BIGINT NOT NULL,
    customer_id BIGINT NOT NULL,
    title varchar(255) NOT NULL,
    description varchar(1024),
    price numeric(10,2) NOT NULL,
    is_deleted boolean NOT NULL,
    created_at timestamp without time zone NOT NULL,
    modified_at timestamp without time zone,
    PRIMARY KEY (id)
);

CREATE INDEX ON public.products
    (customer_id);

CREATE SEQUENCE hibernate_sequence