package ru.appsmart.testtask.controllers

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import ru.appsmart.testtask.models.Customer
import ru.appsmart.testtask.services.CustomerService
import java.time.LocalDateTime


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner::class)
class CustomerControllerTest {

    @Autowired
    lateinit var customerService: CustomerService

    @Autowired
    lateinit var webContext: WebApplicationContext

    @Autowired
    lateinit var mapper: ObjectMapper

    lateinit var mockMvc: MockMvc

    lateinit var customer: Customer

    @Before
    fun init(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webContext).build()
        val customer = Customer("Alex", LocalDateTime.now())
        this.customer = customerService.addCustomer(customer)
    }

    @After
    fun destroy(){
        customerService.deleteCustomerById(customer.id!!)
    }

    @Test
    fun testCustomerControllerGetCustomerByIdIsOk() {
        mockMvc.perform(get("/customers/${customer.id}")).andExpect(status().isOk())
    }

    @Test
    fun testCustomerControllerAddCustomerIsCreated(){
        val customer = Customer("Egor", LocalDateTime.now())
        mockMvc.perform(post("/customers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isCreated())
    }

    @Test
    fun testCustomerControllerDeleteCustomerByIdIsOk(){
        mockMvc.perform(delete("/customers/${customer.id}")).andExpect(status().isOk())
    }

    @Test
    fun testCustomerControllerUpdateCustomerByIdIsOk(){
        val customer = Customer("Ivan", LocalDateTime.now())
        mockMvc.perform(put("/customers/${this.customer.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(customer)))
                .andExpect(status().isOk())
    }
}